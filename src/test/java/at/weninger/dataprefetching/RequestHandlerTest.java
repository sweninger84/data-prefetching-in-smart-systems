package at.weninger.dataprefetching;

import at.weninger.dataprefetching.model.*;
import at.weninger.dataprefetching.network.DataFetchingService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class RequestHandlerTest extends TestCase{

    @Autowired
    DataFetchingService dataFetchingService;
    @Autowired
    Server server;

    /*@Before
    public void setup(){
        new Thread(server).run();
    }

    @After
    public void breakDown(){
        server.shutDown();
    }*/

    @Test
    public void DummyTestCase(){

    }

    //@Test
    public void testResponseToGetMessage(){
        Message message = new Message();
        String json;
        List<ResponseData> data;
        message.setType(MessageType.GET);
        message.setMid(3023);
        message.setCounter(0);
        message.setDeviceId(3);
        message.setMetadata(jsonParser.jsonToUserMetadata(metadata1));

        NeighborData sender = new NeighborData(3, "localhost", 6666);
        //NeighborData receiver = new NeighborData(2, "192.168.0.31", 5555);
        //NeighborData receiver = new NeighborData(1, "192.168.0.143", 5555);
        NeighborData receiver = new NeighborData(1, "localhost", 5555);
        List<NeighborData> receivers = new ArrayList<>();
        receivers.add(receiver);

        /*System.out.println("No Data IDs specified");
        json = dataFetchingService.fetchData(jsonParser.MessageToJson(message), sender, receivers);
        data = jsonParser.JsonToResponseDataList(json);
        for(ResponseData dataItem : data){
            System.out.println("RBL: " + dataItem.getId());
            System.out.println("Content: " + dataItem.getContent());
        }


        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        System.out.println("The following data IDs specified: 2972, 224, 1357, 8834, 657");
        List<Integer> fetchData = new ArrayList<>();
        fetchData.add(2972);
        fetchData.add(224);
        fetchData.add(1357);
        fetchData.add(8834);
        message.setFetchData(fetchData);
        json = dataFetchingService.fetchData(jsonParser.MessageToJson(message), sender, receivers);
        data = jsonParser.JsonToResponseDataList(json);
        for(ResponseData dataItem : data){
            System.out.println("RBL: " + dataItem.getId());
            System.out.println("Content: " + dataItem.getContent());
        }

    }

    //@Test
    public void testResponseToFetchMessage(){
        Message message = new Message();
        message.setType(MessageType.FETCH);
        message.setMid(3003);
        message.setCounter(0);
        message.setDeviceId(3);
        message.setMetadata(jsonParser.jsonToUserMetadata(metadata1));
        List<Integer> fetchData = new ArrayList<>();
        fetchData.add(2972);
        fetchData.add(224);
        fetchData.add(1357);
        fetchData.add(8834);
        fetchData.add(657);
        fetchData.add(1348);
        message.setFetchData(fetchData);

        NeighborData receiver = new NeighborData(1, "localhost", 5555);
        //NeighborData receiver = new NeighborData(2, "192.168.0.31", 5555);

        dataFetchingService.messageNextDevice(receiver, jsonParser.MessageToJson(message));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        NeighborData sender = new NeighborData(2, "localhost", 7777);
        receiver = new NeighborData(1, "localhost", 6666);
        //receiver = new NeighborData(2, "192.168.0.31", 6666);
        List<NeighborData> receivers = new ArrayList<>();
        receivers.add(receiver);

        String response = dataFetchingService.fetchData(metadata1, sender, receivers);
        List<ResponseData> responseData = jsonParser.JsonToResponseDataList(response);
        for(ResponseData item : responseData){
            System.out.print(item.getId() + ": ");
            System.out.println(item.getContent());
        }
    }

    //@Test
    public void testResponseToUserMessage(){
        NeighborData sender = new NeighborData(0, "localhost", 7777);
        NeighborData receiver = new NeighborData(1, "localhost", 6666);
        //NeighborData receiver = new NeighborData(1, "192.168.1.1", 6666);
        //NeighborData receiver = new NeighborData(2, "192.168.0.31", 6666);
        List<NeighborData> receivers = new ArrayList<>();
        receivers.add(receiver);

        String response = dataFetchingService.fetchData(metadata2, sender, receivers);
        List<ResponseData> responseData = jsonParser.JsonToResponseDataList(response);
        for(ResponseData item : responseData){
            System.out.print(item.getId() + ": ");
            System.out.println(item.getContent());
        }
    }

}
