package at.weninger.dataprefetching;

import at.weninger.dataprefetching.model.ResponseData;
import at.weninger.dataprefetching.model.UserMetadata;
import at.weninger.dataprefetching.services.DataPredictionService;
import at.weninger.dataprefetching.util.JsonParser;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class DataPredictionServiceTest extends TestCase {

    @Autowired
    private DataPredictionService dataPredictionService;


    @Test
    public void testGetData(){
        UserMetadata userMetadata = jsonParser.jsonToUserMetadata(metadata1);
        List<Integer> fetchDate = new ArrayList<>();
        /*fetchDate.add(8869);
        fetchDate.add(224);
        fetchDate.add(230);
        fetchDate.add(8833);
        fetchDate.add(2973);

        fetchDate.add(2979);
        fetchDate.add(2970);
        fetchDate.add(2976);
        fetchDate.add(223);
        fetchDate.add(231);*/

        fetchDate.add(549);
        fetchDate.add(1366);
        fetchDate.add(1364);
        fetchDate.add(1367);
        fetchDate.add(1363);
        List<ResponseData> data = dataPredictionService.getData(userMetadata, fetchDate);
        for(ResponseData dataItem : data){
            System.out.println("RBL: " + dataItem.getId());
            System.out.println("Content: " + dataItem.getContent());
        }
    }
}
