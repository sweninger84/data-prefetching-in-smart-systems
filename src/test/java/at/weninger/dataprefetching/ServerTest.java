package at.weninger.dataprefetching;

import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import static org.junit.Assert.*;

/**
 * Unit test for simple Server.
 */
public class ServerTest extends TestCase{
    @Value("${device.id}")
    int deviceId;

    @Autowired
    Server server;

    @Test
    public void testMessageId() throws Exception{
        for(int i = 0; i < 999; i++){
            server.getNextMessageId();
        }
        assertTrue(server.getNextMessageId() == deviceId * 1000 +1);
        assertFalse(server.seenMessageId(deviceId * 1000 + 950));
        assertTrue(server.seenMessageId(deviceId * 1000 + 951));
    }
}
