package at.weninger.dataprefetching;

import at.weninger.dataprefetching.services.UserMobilityPredictionService;
import at.weninger.dataprefetching.util.JsonParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;


public class MobilityPredictionServiceTest extends TestCase{

    private String metadata = "{" +
            "userId: '1'," +
            "currentPos:" +
            "{" +
            "lat: '47.6303237'," +
            "lon: '13.1525936'" +
            "}," +
            "destinyPos:" +
            "{" +
            "lat: '48.2208286'," +
            "lon: '16.339976'" +
            "}," +
            "userType: '0'," +
            "directionInfo: '4'" +
            "}";

    private String metadata2 = "{" +
            "userId: '1'," +
            "currentPos:" +
            "{" +
            "lat: '47.6303237'," +
            "lon: '13.1525936'" +
            "}," +
            "destinyPos:" +
            "{" +
            "lat: '48.2208286'," +
            "lon: '16.339976'" +
            "}," +
            "userType: '0'," +
            "directionInfo: '0'," +
            "movementSpeed: '0.8'" +
            "}";

    @Autowired
    UserMobilityPredictionService userMobilityPredictionService;
    @Autowired
    Server server;

    @Before
    public void setup(){
        new Thread(server).run();
    }

    @After
    public void breakDown(){
        server.shutDown();
    }

    @Test
    public void testGetNextDevice(){
        assertThat(userMobilityPredictionService.getNextDevice(jsonParser.jsonToUserMetadata(metadata2)).getId(), is(2));
        assertThat(userMobilityPredictionService.getNextDevice(jsonParser.jsonToUserMetadata(metadata)), nullValue());
    }

    @Test
    public void getConnectionDurationTest(){
        System.out.println("Duration: " + userMobilityPredictionService.getConnectionDuration(jsonParser.jsonToUserMetadata(metadata2)));
    }
}
