package at.weninger.dataprefetching;

import at.weninger.dataprefetching.util.JsonParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfiguration.class})
public abstract class TestCase {

    @Autowired
    protected JsonParser jsonParser;

    protected String metadata1 = "{" +
            "userId: '15'," +
            "currentPos:" +
            "{" +
            "lat: '47.6303237'," +
            "lon: '13.1525936'" +
            "}," +
            "destinationPos:" +
            "{" +
            "lat: '48.22'," +
            "lon: '16.34'" +
            "}," +
            "userType: '0'" +
            "}";

    protected String metadata1_ts = "{" +
            "userId: '15'," +
            "currentPos:" +
            "{" +
            "lat: '47.6303237'," +
            "lon: '13.1525936'" +
            "}," +
            "destinationPos:" +
            "{" +
            "lat: '48.22'," +
            "lon: '16.34'" +
            "}," +
            "userType: '0'," +
            "timestamp: '2017-12-03T10:15:30'" +
            "}";

    protected String metadata2 = "{" +
            "userId: '15'," +
            "currentPos:" +
            "{" +
            "lat: '47.6303237'," +
            "lon: '13.1525936'" +
            "}," +
            "destinationPos:" +
            "{" +
            "lat: '48.22'," +
            "lon: '16.37'" +
            "}," +
            "userType: '0'" +
            "}";

    protected String metadata3 = "{" +
            "userId: '15'," +
            "directionInfo: '4'," +
            "currentPos:" +
            "{" +
            "lat: '47.6303237'," +
            "lon: '13.1525936'" +
            "}," +
            "destinationPos:" +
            "{" +
            "lat: '48.249'," +
            "lon: '16.45'" +
            "}," +
            "userType: '0'" +
            "}";

    protected String metadata4 = "{" +
            "userId: '15'," +
            "currentPos:" +
            "{" +
            "lat: '47.6303237'," +
            "lon: '13.1525936'" +
            "}," +
            "destinationPos:" +
            "{" +
            "lat: '48.17'," +
            "lon: '16.32'" +
            "}," +
            "userType: '0'" +
            "}";

    protected String metadata5 = "{" +
            "userId: '15'," +
            "currentPos:" +
            "{" +
            "lat: '47.6303237'," +
            "lon: '13.1525936'" +
            "}," +
            "destinationPos:" +
            "{" +
            "lat: '48.17'," +
            "lon: '16.37'" +
            "}," +
            "userType: '0'" +
            "}";

    protected String metadata6 = "{" +
            "userId: '15'," +
            "currentPos:" +
            "{" +
            "lat: '47.6303237'," +
            "lon: '13.1525936'" +
            "}," +
            "destinationPos:" +
            "{" +
            "lat: '48.17'," +
            "lon: '16.43'" +
            "}," +
            "userType: '0'" +
            "}";

}
