package at.weninger.dataprefetching;

import at.weninger.dataprefetching.model.ForDevice;
import at.weninger.dataprefetching.services.TimePredictionService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static junit.framework.TestCase.assertTrue;


public class TimePredictionServiceTest extends TestCase {
    @Autowired
    private TimePredictionService timePredictionService;

    @Test
    public void calcFetchDataTest(){
        String userId = "12";
        List<Integer> dataIds = new ArrayList<>();
        dataIds.add(231);
        dataIds.add(1234);
        dataIds.add(897);
        dataIds.add(7896);
        dataIds.add(456);
        dataIds.add(231);
        dataIds.add(1234);
        dataIds.add(897);
        dataIds.add(7896);
        dataIds.add(456);
        dataIds.add(231);
        dataIds.add(1234);
        dataIds.add(897);
        dataIds.add(7896);
        dataIds.add(456);
        dataIds.add(231);
        dataIds.add(1234);
        dataIds.add(897);
        dataIds.add(7896);
        dataIds.add(456);
        long connectionDuration = 12500;

        Map<ForDevice, List<Integer>> results = timePredictionService.calcFetchData(userId, dataIds, connectionDuration, 500, 3000, 1);
        System.out.println("Ids for this device: " + results.get(ForDevice.THIS).size());
        System.out.println("Ids for next device: " + results.get(ForDevice.NEXT).size());

    }

    @Test
    public void calcFetchTimeTest(){
        List<Integer> dataIds = new ArrayList<>();
        dataIds.add(231);
        dataIds.add(1234);
        dataIds.add(897);
        dataIds.add(7896);

        long result = timePredictionService.calcFetchTime(dataIds, 10000, 1000, 1000);
        assertTrue(result == 8000);
        System.out.println("Time in ms until prefetching needs to start: " + result);

        result = timePredictionService.calcFetchTime(dataIds, 10000, -1000, 1000);
        assertTrue(result == 0);
        System.out.println("Time in ms until prefetching needs to start: " + result);
    }
}
