package at.weninger.dataprefetching;

import at.weninger.dataprefetching.model.ForDevice;
import at.weninger.dataprefetching.model.NeighborData;
import at.weninger.dataprefetching.model.ResponseData;
import at.weninger.dataprefetching.model.UserMetadata;
import at.weninger.dataprefetching.network.DeviceRequestListener;
import at.weninger.dataprefetching.network.UserRequestListener;
import at.weninger.dataprefetching.util.ConfigReader;
import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Queues;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * Copyright 2018 Sabine Weninger
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 *
 * This class provides the program entry point.
 * It generated two RequestListener Threads. One for requests from the user and one for messages from other IoT devices.
 * In order to provide the correct data, it calls several helper services with specialized functionality.
 *
 */
@Component
public class Server implements Runnable, DataStorage{
    private final static Logger LOGGER = Logger.getLogger(Server.class.getName());
    private final int MAXMESSAGEID = 1000;
    private final int MAXMESSAGESTORAGESIZE = 50;
    private final boolean PRODUCTION = false;

    private static int deviceId = 1;

    private int currentMessageId = 0;

    /* Message Ids of the last x messages */
    private Queue<Integer> messageIds = Queues.synchronizedQueue(EvictingQueue.create(MAXMESSAGESTORAGESIZE));
    /* Neighbors data */
    List<NeighborData> neighborsData;
    /* saves Metadata from all users */
    private ConcurrentHashMap<String, UserMetadata> userMetadata = new ConcurrentHashMap<>();
    /* saves data ids for all users in pairs of user id, data id (in our case rbl numbers) */
    private ConcurrentHashMap<String, Map<ForDevice, List<Integer>>> dataKeys = new ConcurrentHashMap<>();
    /* saves prefetched data in pairs of data key, data */
    private ConcurrentHashMap<Integer, String> dataStorage = new ConcurrentHashMap<>();
    /* saves Threads that are currently collecting data for a user in pairs of user id, Thread */
    private ConcurrentHashMap<String, FutureTask<Map<ForDevice, List<Integer>>>> prefetchingHandlerStorage = new ConcurrentHashMap<>();

    @Autowired
    private ConfigReader configReader;
    @Autowired
    private UserRequestListener userRequestListener;
    @Autowired
    private DeviceRequestListener deviceRequestListener;


    public static void main( String[] args ) throws InterruptedException {
        try {
            if(args.length > 0){
                deviceId = Integer.parseInt(args[0]);
            }else {
                LOGGER.warn("Device Id was not given. Using 1.");
            }
        }catch (NumberFormatException e){
            LOGGER.warn("Device Id was not given. Using 1.");
        }

        SpringConfiguration.setDeviceId(""+deviceId);
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        ((Server) context.getBean("server")).run();
    }

    @Override
    public void run() {
        this.configReader.setReader("/neighbors"+deviceId+".config");
        this.neighborsData = configReader.getNeighborAddresses();

        new Thread(userRequestListener).start();
        new Thread(deviceRequestListener).start();

        new Thread(new ServerInputListener()).start();

    }


    public void shutDown(){
        LOGGER.info("Shutting down Server");
        userRequestListener.close();
        deviceRequestListener.close();
    }


    /* Methods for correct message generation */
    @Override
    public void addMessageId(Integer id){
        messageIds.add(id);
    }

    @Override
    public synchronized boolean seenMessageId(Integer id){
        return messageIds.contains(id);
    }

    @Override
    public synchronized Integer getNextMessageId(){
        if(currentMessageId >= MAXMESSAGEID-1){
            currentMessageId = 0;
        }
        currentMessageId++;

        addMessageId(currentMessageId+(deviceId* MAXMESSAGEID));
        return currentMessageId+(deviceId* MAXMESSAGEID);
    }


    /* Access methods for data structures */
    @Override
    public List<NeighborData> getNeighborsData(){
        return this.neighborsData;
    }

    @Override
    public UserMetadata getUserMetadata(String key){
        if(key == null){
            return null;
        }
        return this.userMetadata.get(key);
    }

    @Override
    public void addUserMetadata(String key, UserMetadata userMetadata){
        if(key != null && userMetadata != null){
            this.userMetadata.put(key, userMetadata);
        }
    }

    @Override
    public Map<ForDevice, List<Integer>> getDataKeys(String userId){
        return this.dataKeys.get(userId);
    }

    @Override
    public void storeDataKeys(String key, Map<ForDevice, List<Integer>> dataIds){
        if(key != null && dataIds != null){
            this.dataKeys.put(key, dataIds);
        }
    }

    @Override
    public String getSingleItem(Integer key) {
        return this.dataStorage.get(key);
    }

    @Override
    public void storeSingleItem(Integer key, String data) {
        if(key != null && data != null){
            this.dataStorage.put(key, data);
        }
    }

    @Override
    public List<ResponseData> getDataByKeys(List<Integer> dataIds){
        List<ResponseData> responseDataList = new ArrayList<>();
        for(Integer id : dataIds){
            String item = getSingleItem(id);
            if(item == null){
                continue;
            }
            ResponseData responseData = new ResponseData(id, item);
            responseDataList.add(responseData);
        }
        return responseDataList;
    }

    @Override
    public FutureTask<Map<ForDevice,List<Integer>>> getPrefetchingHandler(String userId) {
        return prefetchingHandlerStorage.get(userId);
    }

    @Override
    public void setPrefetchingHandler(String userId, FutureTask<Map<ForDevice, List<Integer>>> handlerFuture) {
        prefetchingHandlerStorage.put(userId, handlerFuture);
    }

    @Override
    public boolean deletePrefetchingHandler(String userId, FutureTask<Map<ForDevice, List<Integer>>> handlerFuture) {
        return prefetchingHandlerStorage.remove(userId, handlerFuture);
    }

    private class ServerInputListener implements Runnable{

        @Override
        public void run() {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            String s;
            try {
                while ((s = in.readLine()) != null && s.length() != 0) {
                    if(s.equals("close")){
                        shutDown();
                        in.close();
                        return;
                    }
                }
            }catch (IOException e){
                e.printStackTrace();
                try {
                    in.close();
                } catch (IOException e1) {
                    //Can't handle that
                }
            }
        }
    }
}
