package at.weninger.dataprefetching.util;

import at.weninger.dataprefetching.model.GPSbounds;
import at.weninger.dataprefetching.model.NeighborData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Component
public class ConfigReader {

    private BufferedReader reader = null;

    public void setReader(String path){
        if(this.reader != null){
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(path)));
    }

    public List<NeighborData> getNeighborAddresses(){
        if(reader == null){
            return null;
        }
        try{
            List<NeighborData> list = new LinkedList<>();
            String line;
            while (true){
                line = reader.readLine();

                //End of file. Return list.
                if(line == null){
                    return list;
                }

                try {
                    NeighborData neighborData = new NeighborData(line);
                    list.add(neighborData);
                }catch (IllegalArgumentException e){
                    System.out.println("Malformed port");
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                this.reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public ArrayList<GPSbounds> getGPSbounds(){
        if(reader == null){
            return null;
        }
        try{
            ArrayList<GPSbounds> list = new ArrayList<>();
            String line;
            while (true){
                line = reader.readLine();

                //End of file. Return list.
                if(line == null){
                    return list;
                }

                try {
                    GPSbounds bounds = new GPSbounds(line);
                    list.add(bounds);
                }catch (IllegalArgumentException e){
                    System.out.println(e.getMessage());
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                this.reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}
