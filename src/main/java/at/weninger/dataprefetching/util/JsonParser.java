package at.weninger.dataprefetching.util;

import at.weninger.dataprefetching.model.Message;
import at.weninger.dataprefetching.model.ResponseData;
import at.weninger.dataprefetching.model.UserMetadata;
import at.weninger.dataprefetching.model.WienerLinienStationInfo;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.sun.org.apache.regexp.internal.RE;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class JsonParser {
    private final static Logger LOGGER = Logger.getLogger(JsonParser.class.getName());
    private Gson gson;

    public JsonParser(){
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
            @Override
            public LocalDateTime deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                return LocalDateTime.parse(json.getAsString());
            }
        });
        builder.registerTypeAdapter(LocalDateTime.class, new JsonSerializer<LocalDateTime>() {
            @Override
            public JsonElement serialize(LocalDateTime dateTime, Type type, JsonSerializationContext jsonSerializationContext) throws JsonParseException {
                return new JsonPrimitive(dateTime.toString());
            }
        });
        gson = builder.create();
    }

    public UserMetadata jsonToUserMetadata(String json){
        try {
            return gson.fromJson(json, UserMetadata.class);
        }catch(JsonSyntaxException se){
            LOGGER.warn("Could not convert json to UserMetadata Object");
        }
        return null;
    }

    public String userMetadataToJson(UserMetadata userMetadata){
            return gson.toJson(userMetadata);
    }

    public Message jsonToMessage(String json){
        try {
            return gson.fromJson(json, Message.class);
        }catch(JsonSyntaxException se){
            LOGGER.warn("Could not convert json to Message Object");
            se.printStackTrace();
        }
        return null;
    }

    public String MessageToJson(Message message){
        return gson.toJson(message);
    }

    public WienerLinienStationInfo[] jsonToWienerLinienData(JsonReader reader){
        return gson.fromJson(reader, WienerLinienStationInfo[].class);
    }

    public String ResponseDataListToJson(List<ResponseData> dataList){
        Type listType = new TypeToken<List<ResponseData>>() {}.getType();
        return gson.toJson(dataList, listType);
    }

    public List<ResponseData> JsonToResponseDataList(String json){
        Type listType = new TypeToken<List<ResponseData>>() {}.getType();
        return gson.fromJson(json, listType);
    }
}
