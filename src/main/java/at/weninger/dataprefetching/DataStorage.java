package at.weninger.dataprefetching;

import at.weninger.dataprefetching.model.ForDevice;
import at.weninger.dataprefetching.model.NeighborData;
import at.weninger.dataprefetching.model.ResponseData;
import at.weninger.dataprefetching.model.UserMetadata;
import at.weninger.dataprefetching.network.RequestHandler;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * Interface for storing the following:
 * user metadata, collected data for a user
 * PrefetchingHandler thread currently in the process of prefetching data for a user
 * As well as methods for correct message passing between IoT devices
 */
public interface DataStorage {

    /**
     * Adds the given id to a data structure, storing the last x message ids
     * @param id - the message id
     */
    void addMessageId(Integer id);

    /**
     * @param messageId the message id
     * @return true if this message id is within the last x message ids received by this device.
     */
    boolean seenMessageId(Integer messageId);

    /**
    * @return the next message id, which was not used so far
     */
    Integer getNextMessageId();

    /**
    * @return the data (id, address, port) of all the neighbors of this device
     */
    List<NeighborData> getNeighborsData();

    /**
     * @param  userId the user id
    * @return the user metadata for this user id
     */
    UserMetadata getUserMetadata(String userId);

    /**
    * Stores the user metadata, using the user id as key
     * @param userId the user id
     * @param userMetadata the user metadata
     */
    void addUserMetadata(String userId, UserMetadata userMetadata);

    /**
     * @param userId: the user id
    * @return all data keys associated with this user id, separated into two lists ("ForDevice.THIS": items to be sent by the current
     * device; "ForDevice.NEXT": items to be sent by the next device
     */
    Map<ForDevice, List<Integer>> getDataKeys(String userId);

    /**
    * Stores all data ids associated with one user
    * @param userId: the user id
    * @param dataIds: a list of all the data Ids associated with this user, separated into two lists ("ForDevice.THIS": items to
     *               be sent by the current device; "ForDevice:NEXT": items to be sent by the next device
     */
    void storeDataKeys(String userId, Map<ForDevice, List<Integer>> dataIds);

    /**
    * @param dataId: The data Id that identifies data in the data storage
     * @return Returns a single item from the data storage
     */
    String getSingleItem(Integer dataId);

    /**
    * Stores a single data item in the data storage
    * @param dataId: a key which uniquely identifies this data item
    * @param data: the data to be stored
     */
    void storeSingleItem(Integer dataId, String data);

    /**
    * Loops through the dataIds, retrieves the data item and returns a list of ResponseData objects
    * @param dataIds: a list of keys which uniquely identifies each data item
     * @return the list of ResponseData objects
     */
    List<ResponseData> getDataByKeys(List<Integer> dataIds);

    /**
     * @param userId the user id
     * @return returns a PrefetchingHandler FutureTask that is currently prefetching data for this user,
     * or null if no data are currently being prefetched for this user
     */
    FutureTask<Map<ForDevice, List<Integer>>> getPrefetchingHandler(String userId);

    /**
     * Stores a PrefetchingHandler Thread
     * @param userId the user id
     * @param separatedDataIds the PrefetchingHandler FutureTask to be stored
     */
    void setPrefetchingHandler(String userId, FutureTask<Map<ForDevice, List<Integer>>> separatedDataIds);

    /**
     * Removes the given thread from storage
     * @param userId the user id
     * @param separatedDataIds the FutureTask to be removed
     * @return true if successful, false otherwise
     */
    boolean deletePrefetchingHandler(String userId, FutureTask<Map<ForDevice, List<Integer>>> separatedDataIds);


}
