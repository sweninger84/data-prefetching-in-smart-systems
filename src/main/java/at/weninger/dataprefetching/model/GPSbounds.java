package at.weninger.dataprefetching.model;

/**
 * Model class for storing GPS coordinates
 * the coordinates which divide Vienna into 6 region for the six rasperry Pis used in this project
 */
public class GPSbounds {
    private double latMin;
    private double latMax;
    private double lngMin;
    private double lngMax;

    public GPSbounds(String allValues){
        setBounds(allValues);
    }

    public GPSbounds(double latMin, double latMax, double lngMin, double lngMax){
        this.latMin = latMin;
        this.latMax = latMax;
        this.lngMin = lngMin;
        this.lngMax = lngMax;
    }

    public double getLatMin() {
        return latMin;
    }

    public double getLatMax() {
        return latMax;
    }

    public double getLngMin() {
        return lngMin;
    }

    public double getLngMax() {
        return lngMax;
    }

    private void setBounds(String allValues) throws IllegalArgumentException{
        String[] parts = allValues.split(" ");
        if(parts.length != 2){
            throw new IllegalArgumentException("Malformed GPS bounds.");
        }
        String[] latParts = parts[0].split("-");
        String[] lngParts = parts[1].split("-");
        if(latParts.length != 2 || lngParts.length != 2){
            throw new IllegalArgumentException("Malformed GPS bounds.");
        }
        try{
            this.latMin = Double.parseDouble(latParts[0]);
            this.latMax = Double.parseDouble(latParts[1]);
            this.lngMin = Double.parseDouble(lngParts[0]);
            this.lngMax = Double.parseDouble(lngParts[1]);
        }catch (NumberFormatException e){
            e.printStackTrace();
        }
    }
}
