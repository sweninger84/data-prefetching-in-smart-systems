package at.weninger.dataprefetching.model;

/**
 * Model class for data necessary to reach neighboring IOT devices (contains the IP address and port)
 */
public class NeighborData {
    private int id;
    private String address;
    private int port;

    public NeighborData(int id, String address, int port){
        this.id = id;
        this.address = address;
        this.port = port;
    }

    public NeighborData(String fullAddress) throws IllegalArgumentException{
        parseAddress(fullAddress);
    }

    public String getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    public int getId() { return id; }

    @Override
    public boolean equals(Object other){
        // self check
        if (this == other)
            return true;
        // null check
        if (other == null)
            return false;
        // type check and cast
        if (getClass() != other.getClass())
            return false;

        NeighborData otherData = (NeighborData) other;
        return this.id == otherData.id && (otherData.address.contains(this.address) || this.address.contains(otherData.address));
    }

    private void parseAddress(String fullAddress) throws IllegalArgumentException{
        String[] parts = fullAddress.split(":");
        if(parts.length != 3){
            System.out.println("Malformed information");
            throw new IllegalArgumentException("Malformed IP address");
        }
        try {
            this.id = Integer.parseInt(parts[0]);
            this.address = parts[1];
            this.port = Integer.parseInt(parts[2]);
        }catch (NumberFormatException e){
            System.out.println("Malformed port");
            throw new IllegalArgumentException("Could not read Port");
        }
    }
}
