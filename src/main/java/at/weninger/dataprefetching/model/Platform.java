package at.weninger.dataprefetching.model;

/**
 * Model class for Wiener Linien specific data
 */
public class Platform {
    private String line;
    private int rbl;

    public String getLine() {
        return line;
    }

    public int getRbl() {
        return rbl;
    }

}
