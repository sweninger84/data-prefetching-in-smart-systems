package at.weninger.dataprefetching.model;

/**
 * Model class for Wiener Linien specific data
 */
public class WienerLinienStationInfo {
    private double latitude;
    private double longitude;
    private String name;
    private String relatedLines;
    private String stationID;
    private Platform[] platforms;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }

    public String getRelatedLines() {
        return relatedLines;
    }

    public String getStationID() {
        return stationID;
    }

    public Platform[] getPlatforms() {
        return platforms;
    }
}
