package at.weninger.dataprefetching.model;

/**
 * The MessageType of a message passed between IOT devices
 */
public enum MessageType {
    GET, FETCH
}
