package at.weninger.dataprefetching.model;

/**
 * Model class for a GPS position
 */
public class Position {
    private double lat;
    private double lon;

    public Position() {
    }

    public Position(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    @Override
    public boolean equals(Object other) {
        // self check
        if (this == other)
            return true;
        // null check
        if (other == null)
            return false;
        // type check and cast
        if (getClass() != other.getClass())
            return false;

        return this.lat == ((Position) other).lat && this.lon == ((Position) other).lon;
    }
}
