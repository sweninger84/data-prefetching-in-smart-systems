package at.weninger.dataprefetching.model;

/**
 * Model class for data returned from the Wiener Linien API
 */
public class ResponseData {
    //set to -1 if an Error occurred, else this contains the RBL number
    private int id;
    private String content;

    public ResponseData(){

    }

    public ResponseData(int id, String content) {
        this.id = id;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
