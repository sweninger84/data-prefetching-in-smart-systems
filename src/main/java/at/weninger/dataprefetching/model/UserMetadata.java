package at.weninger.dataprefetching.model;

import java.time.LocalDateTime;

/**
 * Model class for the user metadata, which are provided in json format
 */
public class UserMetadata {
    private String userId;
    private Position currentPos;
    private Position destinationPos;
    private int userType;
    // in our case the id of the last device the user encountered
    private int directionInfo = -1;
    private double movementSpeed = -1; //in m/sec
    private LocalDateTime timestamp = LocalDateTime.now();


    public String getUserId() {
        return userId;
    }

    public Position getCurrentPos() {
        return currentPos;
    }

    public Position getDestinationPos() {
        return destinationPos;
    }

    public int getUserType() {
        return userType;
    }

    public int getDirectionInfo() {
        return directionInfo;
    }

    public void setDirectionInfo(int directionInfo) {
        this.directionInfo = directionInfo;
    }

    public double getMovementSpeed() {
        return movementSpeed;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public boolean matches(UserMetadata other){
        return this.userId.equals(other.userId) && this.destinationPos.equals(other.destinationPos) && other.timestamp.isBefore(this.timestamp.plusMinutes(15));
    }

}
