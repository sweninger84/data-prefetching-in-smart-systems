package at.weninger.dataprefetching.model;

import java.util.List;

/**
 * This class represents a message passed between devices.
 * There are two possible types of message. These are:
 * GET messages: sent to another device to request data
 * FETCH messages: sent to another device to inform this device to start fetching data
 */
public class Message {
    private MessageType type;
    private int mid;
    private int counter;
    private int deviceId;
    private List<Integer> fetchData;
    private UserMetadata metadata;
    //saves whether this device received the fetch message from the originating device or from a device which just passed along the message
    boolean next;

    public Message(){}

    public MessageType getType() {
        return type;
    }

    public int getMid() {
        return mid;
    }

    public int getCounter() {
        return counter;
    }

    public List<Integer> getFetchData() {
        return fetchData;
    }

    public UserMetadata getMetadata() {
        return metadata;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public void setFetchData(List<Integer> fetchData) {
        this.fetchData = fetchData;
    }

    public void setMetadata(UserMetadata metadata) {
        this.metadata = metadata;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isNext() {
        return next;
    }

    public void setNext(boolean next) {
        this.next = next;
    }
}
