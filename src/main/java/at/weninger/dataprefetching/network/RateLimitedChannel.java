package at.weninger.dataprefetching.network;

import at.borkowski.spicej.Streams;
import at.borkowski.spicej.rt.RateCalculator;
import at.borkowski.spicej.rt.RealTimeTickSource;

import java.io.*;
import java.net.Socket;

/**
* Channel, which limits the InputStream speed to a given rate (using the SpiceJ library).
* Generates a new TCPChannel, which is used for sending and receiving data using the rate limited
* InputStream and an unlimited OutputStream.
*/
public class RateLimitedChannel implements Channel {
    private TCPChannel tcpChannel;

    public RateLimitedChannel(Socket socket, Float rate) throws IOException{
        tcpChannel = new TCPChannel(socket, rate(socket.getInputStream(), rate), socket.getOutputStream());
    }

    @Override
    public void send(String message) {
        tcpChannel.send(message);
    }

    @Override
    public String receive() throws IOException {
        return tcpChannel.receive();
    }

    @Override
    public void close() {
        tcpChannel.close();
    }

    private static InputStream rate(InputStream inputStream, Float rate) { // rate: bytes pro sekunde
        if (rate == null)
            return inputStream;

        RateCalculator.Result calculation = RateCalculator.calculate(rate);
        return Streams.limitRate(inputStream, new RealTimeTickSource(calculation.getTickNanosecondsInterval(), true), calculation.getBytesPerTick(), calculation.getPrescale());
    }
}
