package at.weninger.dataprefetching.network;

import at.weninger.dataprefetching.DataStorage;
import at.weninger.dataprefetching.model.*;
import at.weninger.dataprefetching.services.DataPredictionService;
import at.weninger.dataprefetching.services.TimePredictionService;
import at.weninger.dataprefetching.services.UserMobilityPredictionService;
import at.weninger.dataprefetching.util.JsonParser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Prefetches or urgently fetches data by calling the DataFetchingService.
 * Needs the user metadata and a list of data ids that need to be fetched (in case this list is null or empty
 * only the data ids (and not the full data) for this user's metadata will be fetched
 */
@Component
@Scope("prototype")
public class PrefetchingHandler implements Callable {
    protected final static Logger LOGGER = Logger.getLogger(PrefetchingHandler.class.getName());

    /**
     * true if method is called by UserRequestHandler, false in case of data prefetching after a FETCH message
     * was received
     */
    private boolean urgent = false;

    private UserMetadata metadata;
    private Map<ForDevice, List<Integer>> separatedDataIds;

    @Autowired
    private DataPredictionService dataPredictionService;
    @Autowired
    private DataStorage dataStorage;
    @Autowired
    private DataFetchingService dataFetchingService;
    @Autowired
    private UserMobilityPredictionService userMobilityPredictionService;
    @Autowired
    private TimePredictionService timePredictionService;
    @Autowired
    private JsonParser jsonParser;

    @Value("${device.id}")
    protected int deviceId;

    //Size of a data item in byte
    @Value("${sizeOfDataItem}")
    protected int sizeOfDataItem;

    //Speed of data connection to the user in byte/sec
    @Value("${transferSpeed}")
    protected int transferSpeed;


    /**
     * Prefetches or urgently fetches data for a user.
     * Needs the Map separatedDataIds to be set.
     * In case the boolean urgent is set: the data are fetched immediately
     * Otherwise the thread sleeps for the time amount calculated by the TimePredictionService.
     * Afterwards the DataPredictionService is called which returns whether the current device
     * has the data available.
     * If the data items are available locally they are retrieved via the DataPredictionService.
     * Otherwise a request (GET message) for these data items is sent to neighboring devices.
     * The received data items are stored in the DataStorage.
     *
     * @return the separatedDataIds
     */
    private Map<ForDevice, List<Integer>> prefetchData(){
        if(separatedDataIds == null){
            return null;
        }
        if(!urgent){
            long waitingTime = timePredictionService.calcFetchTime(separatedDataIds.get(ForDevice.THIS), userMobilityPredictionService.getTimeUntilArrival(metadata), transferSpeed, sizeOfDataItem);
            LOGGER.info("Waiting time before Prefetching: " + waitingTime);
            if(waitingTime > 0){
                try {
                    LOGGER.info("fetchData: starting to sleep");
                    Thread.sleep(waitingTime);
                    LOGGER.info("fetchData: waking up");
                } catch (InterruptedException e) {
                    LOGGER.info("fetchData: Was interrupted." + e.getMessage());
                }
            }
        }

        List<ResponseData> responseData;
        if(dataPredictionService.isResponsible(metadata)){
            //This device needs to fetch the data.
            LOGGER.info("Data available on this device.");
            responseData = dataPredictionService.getData(metadata, separatedDataIds.get(ForDevice.THIS));
        }else{
            //Ask neighboring devices for data
            LOGGER.info("Asking neighbors for data.");
            Message messageForNeighbor = new Message();
            messageForNeighbor.setType(MessageType.GET);
            messageForNeighbor.setDeviceId(deviceId);
            messageForNeighbor.setMid(dataStorage.getNextMessageId());
            messageForNeighbor.setCounter(0);
            messageForNeighbor.setMetadata(metadata);
            messageForNeighbor.setFetchData(separatedDataIds.get(ForDevice.THIS));
            responseData = jsonParser.JsonToResponseDataList(dataFetchingService.fetchData(jsonParser.MessageToJson(messageForNeighbor), null));
            LOGGER.info("Neighbors returned data");
        }
        //Now we store those data
        List<Integer> dataIds = new ArrayList<>();
        for(ResponseData dataItem : responseData){
            if(dataItem.getId() == -1){
                //an error occurred
                LOGGER.warn(dataItem.getContent());
                continue;
            }
            dataIds.add(dataItem.getId());
            if(!dataItem.getContent().isEmpty() && !dataItem.getContent().equals("Data item could not be retrieved.")) {
                dataStorage.storeSingleItem(dataItem.getId(), dataItem.getContent());
            }
        }

        LOGGER.info(responseData.size() + " items were fetched!");
        if(dataIds.size() > 0) {
            separatedDataIds.put(ForDevice.THIS, dataIds);
            dataStorage.storeDataKeys(metadata.getUserId(), separatedDataIds);
            return separatedDataIds;
        }
        return separatedDataIds;
    }

    public void setUrgent(boolean urgent) {
        this.urgent = urgent;
    }

    public void setMetadata(UserMetadata metadata) {
        this.metadata = metadata;
    }

    public void setSeparatedDataIds(Map<ForDevice, List<Integer>> separatedDataIds) {
        this.separatedDataIds = separatedDataIds;
    }

    @Override
    public Object call() throws Exception {
        return prefetchData();
    }
}
