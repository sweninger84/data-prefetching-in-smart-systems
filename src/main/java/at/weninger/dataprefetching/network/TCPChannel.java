package at.weninger.dataprefetching.network;

import java.io.*;
import java.net.Socket;

/**
 * Channel, which sends and receives data over a TCP connection
 */
public class TCPChannel implements Channel{
    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;

    public TCPChannel(Socket socket, InputStream inputStream, OutputStream outputStream) throws IOException{
        this.socket = socket;
        this.out = new PrintWriter(outputStream, true);
        in = new BufferedReader(new InputStreamReader(inputStream));
    }

    @Override
    public void send(String message){
        out.println(message);
    }

    @Override
    public String receive() throws IOException{
        return in.readLine();
    }

    @Override
    public void close(){
        if(this.socket != null && !this.socket.isClosed()){
            try{
                in.close();
                out.close();
                socket.close();
            }catch (IOException e){
                //cannot handle that
            }
        }
    }

}
