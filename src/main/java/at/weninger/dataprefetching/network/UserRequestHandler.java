package at.weninger.dataprefetching.network;

import at.weninger.dataprefetching.model.ForDevice;
import at.weninger.dataprefetching.model.ResponseData;
import at.weninger.dataprefetching.model.UserMetadata;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Handles messages received from a user. Checks if data were prefetched. If they were, those data are
 * returned. Otherwise data are collected and returned. A FETCH message is sent to the next device, to initiate
 * data prefetching.
 */
@Component
@Scope("prototype")
public class UserRequestHandler extends RequestHandler{

    /**
     * Handles a message from a user
     * If necessary a FETCH message is passed on to the next device to initiate data prefetching
     * @param message The message
     * @param senderAddress The address of the user that sent this message
     * @return collected data for this user
     */
    @Override
    protected String handleMessage(String message, InetSocketAddress senderAddress){
        UserMetadata metadata = jsonParser.jsonToUserMetadata(message);
        if(metadata == null || metadata.getUserId() == null){
            LOGGER.warn("Malformed Request Metadata");
            return jsonParser.ResponseDataListToJson(createErrorMessage("Malformed Request Metadata"));
        }

        //if this becomes true, the data for this user need to be fetched
        boolean dataChanged = checkAndStoreUserMetadata(metadata);

        //Check if data for this user were already prefetched, if not fetch them
        Map<ForDevice, List<Integer>> separatedDataIds = dataStorage.getDataKeys(metadata.getUserId());
        if(separatedDataIds == null || separatedDataIds.get(ForDevice.THIS) == null || dataChanged){
            LOGGER.info("Data were not fetched or the metadata changed.");
            separatedDataIds = fetchData(metadata, new HashMap<>());
            if(separatedDataIds == null || separatedDataIds.get(ForDevice.THIS) == null){
                return jsonParser.ResponseDataListToJson(createErrorMessage("No eligible data could be collected."));
            }
        }else{
            LOGGER.info("Data were already prefetched! Returning them now.");
        }

        //Check and if necessary send FETCH message to next device
        if(separatedDataIds.get(ForDevice.NEXT) != null && separatedDataIds.get(ForDevice.NEXT).size() > 0){
            List<Integer> dataIds = separatedDataIds.get(ForDevice.NEXT);
            Runnable runnable = () -> messageNextDevice(metadata, dataIds, false);
            new Thread(runnable).start();
        }

        if(separatedDataIds.get(ForDevice.THIS).size() > 0){
            List<ResponseData> returnData = dataStorage.getDataByKeys(separatedDataIds.get(ForDevice.THIS));
            if(returnData.size() == 0 || dataChanged) {
                //before only the ids were fetched, now fetch the actual data items
                LOGGER.info("Data not prefetched. Need to be fetched now.");
                fetchData(metadata, separatedDataIds);
                returnData = dataStorage.getDataByKeys(separatedDataIds.get(ForDevice.THIS));
            }
            return jsonParser.ResponseDataListToJson(returnData);
        }else{
            //No data ids were collected, return an error message
            return jsonParser.ResponseDataListToJson(createErrorMessage("No eligible data could be collected."));
        }
    }

    private List<ResponseData> createErrorMessage(String message){
        ResponseData responseData = new ResponseData(-1, message);
        List<ResponseData> responseDataList = new ArrayList<>();
        responseDataList.add(responseData);
        return responseDataList;
    }

    /**
     * Saves user metadata. Also checks if metadata for this user were already saved and whether they are identical to the ones the
     * user sent now.
     * @param metadata The metadata sent by the user now
     * @return whether metadata changed - if this is true, present data for this user will be
     * dropped and new data fetched
     */
    private boolean checkAndStoreUserMetadata(UserMetadata metadata){
        boolean dataChanged = false;
        UserMetadata storedMetadata = dataStorage.getUserMetadata(metadata.getUserId());
        if(storedMetadata != null){
            //direction info in our case never comes from the user, so should be overwritten with old data
            LOGGER.info("Overwriting direction Info with: " + storedMetadata.getDirectionInfo());
            metadata.setDirectionInfo(storedMetadata.getDirectionInfo());
        }
        dataStorage.addUserMetadata(metadata.getUserId(), metadata);
        if(storedMetadata == null || !storedMetadata.matches(metadata)){
            dataChanged = true;
        }
        return dataChanged;
    }

    /**
     * Checks if data for this are currently in the process of being fetched.
     * If this is not the case, data fetching is initiated (via a prefetching handler).
     * After the handler finished fetching, the dataIds are separated if necessary and returned.
     * @param metadata the user metadata
     * @param separatedDataIds the ids of the data units that need to be fetched
     * @return the dataIds of the fetched data, separated into ids for the current and for the next device
     */
    private Map<ForDevice, List<Integer>> fetchData(UserMetadata metadata, Map<ForDevice, List<Integer>> separatedDataIds){
        //stores whether data ids have been separated into ids for the current and the next device
        boolean separated = true;
        //Are data currently being fetched?
        FutureTask<Map<ForDevice, List<Integer>>> handlerFuture = dataStorage.getPrefetchingHandler(metadata.getUserId());
        if(handlerFuture == null){
            //No they are not. So generate a new Handler to fetch the data for us.
            LOGGER.info("No Handler available: Start a new one.");
            separated = false;
            PrefetchingHandler handler = super.generateHandler(metadata, separatedDataIds, true);
            handlerFuture = new FutureTask<>(handler);
            new Thread(handlerFuture).start();
        }
        //Now wait for the prefetching Handler to finish
        try{
            LOGGER.info("Waiting for HandlerThread to return.");
            separatedDataIds = handlerFuture.get();
            LOGGER.info("HandlerThread terminated.");
        }catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }finally {
            dataStorage.deletePrefetchingHandler(metadata.getUserId(), handlerFuture);
        }

        if(!separated && separatedDataIds != null){
            //data items were not separated into items for the current and the next device yet, do that now
            separatedDataIds = timePredictionService.calcFetchData(metadata.getUserId(), separatedDataIds.get(ForDevice.THIS),
                    userMobilityPredictionService.getConnectionDuration(metadata), super.transferSpeed,
                    super.sizeOfDataItem, 0);
        }

        return separatedDataIds;
    }

}
