package at.weninger.dataprefetching.network;

import java.io.IOException;

/**
 * Interface providing methods to send and receive data over a provided socket.
 */
public interface Channel {

    /**
     * Sends out data over the provided socket
     * @param message: The message to be sent
     */
    void send(String message);

    /**
     * Waits for an answer (InputStream from the socket)
     * @return: the answer sent by another device
     * @throws IOException
     */
    String receive() throws IOException;

    /**
     * Closes the socket
     */
    void close();
}
