package at.weninger.dataprefetching.network;

import at.weninger.dataprefetching.DataStorage;
import at.weninger.dataprefetching.model.*;
import at.weninger.dataprefetching.services.DataPredictionService;
import at.weninger.dataprefetching.services.TimePredictionService;
import at.weninger.dataprefetching.services.UserMobilityPredictionService;
import at.weninger.dataprefetching.util.JsonParser;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;

/**
 * Manages the request from either a user or a neighboring IoT device. Passes on the message to
 * the corresponding child class.
 */
@Component
@Scope("prototype")
public abstract class RequestHandler extends ChannelInboundHandlerAdapter{
    protected final static Logger LOGGER = Logger.getLogger(RequestHandler.class.getName());
    protected final int MAXHOP = 5;

    @Value("${device.id}")
    protected int deviceId;

    @Value("${next.next}")
    protected boolean passOnFetchMessage;

    //Size of a data item in byte
    @Value("${sizeOfDataItem}")
    protected int sizeOfDataItem;

    //Speed of data connection to the user in byte/sec
    @Value("${transferSpeed}")
    protected int transferSpeed;

    @Autowired
    protected BeanFactory beanFactory;
    @Autowired
    protected DataPredictionService dataPredictionService;
    @Autowired
    protected DataStorage dataStorage;
    @Autowired
    protected DataFetchingService dataFetchingService;
    @Autowired
    protected UserMobilityPredictionService userMobilityPredictionService;
    @Autowired
    protected TimePredictionService timePredictionService;
    @Autowired
    protected JsonParser jsonParser;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        try{
            String message = (String) msg;
            InetSocketAddress sender = (InetSocketAddress) ctx.channel().remoteAddress();
            LOGGER.info("Sender: " + sender.getAddress() + " ... " + sender.getPort());
            LOGGER.info("Message: " + msg);

            ctx.write(handleMessage(message, sender));
            ctx.flush();
            ctx.close();
        }finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        LOGGER.error("Exception caught. Closing connection.");
        cause.printStackTrace();
        ctx.close();
    }

    protected abstract String handleMessage(String data, InetSocketAddress senderddress);


    /**
     * Prepares a PrefetchingHandler
     * @param metadata the user metadata
     * @param dataIds the dataIds (can be null)
     * @param urgent - true if called by UserRequestHandler, false if called by DeviceRequestHandler
     * @return PrefetchingHandler thread
     */
    protected PrefetchingHandler generateHandler(UserMetadata metadata, Map<ForDevice, List<Integer>> dataIds, boolean urgent){
        PrefetchingHandler handler = beanFactory.getBean(PrefetchingHandler.class);
        handler.setMetadata(metadata);
        handler.setSeparatedDataIds(dataIds);
        handler.setUrgent(urgent);
        return handler;
    }

    /**
     * Prepares and sends a FETCH message to the next device
     * Calls the UserMobilityPredictionService to calculate which device the user will reach next.
     * Calls the DataFetchingService to inform the next device.
     * @param metadata: the user metadata
     * @param dataIdsForNextDevice: the data ids that should be provided to the user by the next device
     * @param isNext
     */
    protected void messageNextDevice(UserMetadata metadata, List<Integer> dataIdsForNextDevice, boolean isNext){
        if(metadata == null || dataIdsForNextDevice.size() == 0){
            return;
        }
        NeighborData address = userMobilityPredictionService.getNextDevice(metadata);
        if(metadata.getDirectionInfo() == -1){
            metadata.setDirectionInfo(this.deviceId);
        }
        Message fetchMessage = new Message();
        fetchMessage.setType(MessageType.FETCH);
        fetchMessage.setMetadata(metadata);
        fetchMessage.setDeviceId(this.deviceId);
        fetchMessage.setNext(isNext);
        fetchMessage.setFetchData(dataIdsForNextDevice);
        if(address != null) {
            LOGGER.info("Sending fetch message to device: " + address.getId());
            dataFetchingService.messageNextDevice(address, jsonParser.MessageToJson(fetchMessage));
        }else{
            LOGGER.info("There is no next device. Cannot send message.");
        }
    }
}
