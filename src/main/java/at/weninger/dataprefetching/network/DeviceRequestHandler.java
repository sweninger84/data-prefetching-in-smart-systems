package at.weninger.dataprefetching.network;

import at.weninger.dataprefetching.model.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.FutureTask;

/**
 * Handles message received from other IoT devices, either requesting data (GET message) or notifying
 * this device that a user is coming and data need to be prefetched (FETCH message).
 */
@Component
@Scope("prototype")
public class DeviceRequestHandler extends RequestHandler {

    /**
     * Overrides handleMessage from Request Handler
     * Passes on the message to the correct method (GET or FETCH)
     * @param message: the received message
     * @param senderAddress the address of the sender
     * @return Answer to the request
     */
    @Override
    protected String handleMessage(String message, InetSocketAddress senderAddress) {
        String answer;
        Message deviceMessage = jsonParser.jsonToMessage(message);
        if(deviceMessage == null || deviceMessage.getType() == null){
            answer = "Error on device " + deviceId + ": Device message has no type";
        }
        else if(deviceMessage.getType().equals(MessageType.GET)){
            answer = handleGetMessage(deviceMessage, new NeighborData(deviceMessage.getDeviceId(), senderAddress.getAddress().toString(), senderAddress.getPort()));
        }
        else if(deviceMessage.getType().equals(MessageType.FETCH)){
            answer = handleFetchMessage(deviceMessage);
        }
        else{
            answer = "Error on device " + deviceId + ": Unknown Message Type";
        }
        return answer;
    }

    /**
     * Handles a GET Message from another device. Checks if this device has the requested data.
     * If it does, it immediately returns these data. Otherwise the message is passed on the the
     * neighbors of this devices and their response is returned.
     * @param message: The GET message: usually contains the user metadata and possibly a list of data ids.
     * @param senderAddress: the address of the sender
     * @return The requested data (json encoded)
     */
    private String handleGetMessage(Message message, NeighborData senderAddress){
        boolean error = false;
        List<ResponseData> responseDataList = new ArrayList<>();

        //Check message ID
        if(dataStorage.seenMessageId(message.getMid())){
            //The message has already been seen, we ignore it now
            LOGGER.warn("Message already seen.");
            responseDataList.add(new ResponseData(-1, "Error on device " + deviceId + ": Message already seen"));
            error = true;
        }
        dataStorage.addMessageId(message.getMid());
        //Check counter
        if(message.getCounter() > MAXHOP){
            LOGGER.warn("Max Hop number exceeded");
            responseDataList.add(new ResponseData(-1, "Error on device " + deviceId + ": Max Hop number exceeded"));
            error = true;
        }

        //handle content
        UserMetadata metadata = message.getMetadata();
        if(metadata == null){
            LOGGER.warn("Malformed Request Metadata");
            responseDataList.add(new ResponseData(-1, "Error on device " + deviceId + ": Malformed Request Metadata"));
            error = true;
        }

        if(error){
            return jsonParser.ResponseDataListToJson(responseDataList);
        }

        /* Check if this device is responsible for collecting the data. */
        if(dataPredictionService.isResponsible(metadata)){
            //This device is responsible
            LOGGER.info("Device " + deviceId + " is responsible. Returning data.");
            responseDataList = dataPredictionService.getData(metadata, message.getFetchData());
        }else {
            try{
                //Ask neighbors for data
                LOGGER.info("Data not on device. Passing on the message.");
                message.setCounter(message.getCounter()+1);
                return dataFetchingService.fetchData(jsonParser.MessageToJson(message), senderAddress);
            }catch (IllegalArgumentException e){
                //Should not happen
                LOGGER.info("Address of Sender malformed");
                responseDataList.add(new ResponseData(-1, "Error on device " + deviceId + ": Address of Sender is malformed"));
            }
        }
        return jsonParser.ResponseDataListToJson(responseDataList);

    }

    /**
     * Handles a FETCH message from another device. This message contains the user metadata and a lists of data ids
     * for the data to be prefetched. This method first separates the data ids into ids that need to be fetched by this
     * device and ids that will be passed on to the next device. Then it starts a new PrefetchingHandler thread to
     * fetch the necessary data items.
     * @param message: the FETCH message from another device
     * @return Information String that this device is now prefetching data.
     */
    private String handleFetchMessage(Message message){
        UserMetadata metadata = message.getMetadata();
        List<Integer> fetchIds = message.getFetchData();

        if(metadata == null || fetchIds == null){
            LOGGER.warn("Error on device " + deviceId + ": Could not prefetch! User metadata empty or no dataIds specified.");
            return "Error on device " + deviceId + ": Could not prefetch! User metadata empty or no dataIds specified.";
        }

        dataStorage.addUserMetadata(metadata.getUserId(), metadata);

        //determine which data items should be fetched by this device
        Map<ForDevice, List<Integer>> separatedDataIds = timePredictionService.calcFetchData(metadata.getUserId(), fetchIds,
                userMobilityPredictionService.getConnectionDuration(metadata), super.transferSpeed, super.sizeOfDataItem, 1);
        if(separatedDataIds.get(ForDevice.THIS).size() > 0){
            LOGGER.info("Device " + deviceId + ": Fetching data");
            PrefetchingHandler handler = super.generateHandler(metadata, separatedDataIds, false);
            FutureTask<Map<ForDevice, List<Integer>>> future = new FutureTask<>(handler);
            dataStorage.setPrefetchingHandler(metadata.getUserId(), future);
            new Thread(future).start();
        }

        return "Device " + deviceId + ": Fetching data";
    }
}
