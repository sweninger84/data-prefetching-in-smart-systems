package at.weninger.dataprefetching.network;

import at.weninger.dataprefetching.DataStorage;
import at.weninger.dataprefetching.model.NeighborData;
import at.weninger.dataprefetching.model.ResponseData;
import at.weninger.dataprefetching.util.ConfigReader;
import at.weninger.dataprefetching.util.JsonParser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * This class provides methods to communicate with other IoT devices.
 * Communication between IoT devices is implemented in a distributed manner,
 * meaning that there is no central unit coordinating traffic between the devices.
 * Each device has means to communicate with neighbouring devices.
 * So data stored in a device further away may need to pass several links before reaching the destination device.
 */
@Component
@Scope("prototype")
public class DataFetchingService {
    private final static Logger LOGGER = Logger.getLogger(DataFetchingService.class.getName());

    @Value("${device.id}")
    private int deviceId;

    @Value("${rate}")
    private float rate;

    /**
     * Connection data to neighboring devices
     */
    private List<NeighborData> neighborsData;

    @Autowired
    private ConfigReader configReader;
    @Autowired
    private DataStorage dataStorage;
    @Autowired
    private JsonParser jsonParser;

    /**
     *  Fetches the data
     *  Sends the given message to all the neighbors and returns their response
     *
     *  @param message: The message to be sent (already json encoded)
     *  @param sender: The connection data of this device (the sender)
     *  @param neighbors: a list of connection data (IP addresses) to which the message will be sent
     *
     *  @return
     *      The first valid response data, received from one of the neighboring devices
     */
    public String fetchData(String message, NeighborData sender, List<NeighborData> neighbors){
        ArrayList<String> answers = new ArrayList<>();
        int poolsize = neighbors.size();
        ExecutorService pool = Executors.newFixedThreadPool(poolsize);
        List<Callable<String>> tasks = new ArrayList<>();
        for(NeighborData neighbor : neighbors){
            if(neighbor == null || (sender != null && sender.equals(neighbor))){
                continue;
            }
            try {
                Callable<String> handler = new DataFetchingHandler(neighbor, rate, message);
                tasks.add(handler);
            }catch (IOException e){
                LOGGER.error("Could not open socket to IP: " + neighbor.getAddress());
            }
        }
        try {
            List<Future<String>> results = pool.invokeAll(tasks);

            for (Future<String> future : results) {
                String answer = future.get();
                if(answer != null){
                    answers.add(answer);
                }
            }
        }catch (InterruptedException | ExecutionException e){
            LOGGER.error("Error while contacting neighbors!" );
            e.printStackTrace();
        }finally {
            pool.shutdown();
        }

        List<ResponseData> responseDataList;
        for(String answer : answers){
            responseDataList = jsonParser.JsonToResponseDataList(answer);
            if(responseDataList == null || responseDataList.size() == 0){
                LOGGER.info("No data were collected.");
                continue;
            }else if(responseDataList.get(0).getId() == -1){
                LOGGER.info("An error occurred: " + responseDataList.get(0).getContent());
                continue;
            }
            return answer;
        }
        // no answer collected -> return ResponseData with error message
        ResponseData responseData = new ResponseData(-1, "No eligible data could be collected.");
        responseDataList = new ArrayList<>();
        responseDataList.add(responseData);
        return jsonParser.ResponseDataListToJson(responseDataList);
    }

   /**
   * Calls fetchData providing the Neighbor data from the properties file
    * @param message the message to be sent
    * @param sender the sender of this message
    * @return the response from the neighbors
    */
    public String fetchData(String message, NeighborData sender) {
        if(neighborsData == null){
            neighborsData = dataStorage.getNeighborsData();
        }
        return fetchData(message, sender, this.neighborsData);
    }


    /**
     * Messages the next IoT Device that a user is coming.
     * By passing information on what data need to be provided to the user by the next device (which do not
     * include data that have already been fetched by the current device), it is made sure that the user
     * does not receive the same data from multiple devices.
     * @param next: The connection data for the next device
     * @param message: The message to be sent to the next device (in json format)
     */
    public void messageNextDevice(NeighborData next, String message){
        if(next == null || message == null || message.isEmpty()){
            return;
        }
        Channel channel = null;
        try{
            Socket socket = new Socket(next.getAddress(), next.getPort());
            channel = new TCPChannel(socket, socket.getInputStream(), socket.getOutputStream());
            channel.send(message);
            String answer = channel.receive();
            if (answer.startsWith("ERROR")) {
                LOGGER.warn(answer);
            }
            channel.close();
        }catch (IOException e){
            LOGGER.error("Error while writing or reading to/from: " + next.getAddress());
            if(channel != null)
                channel.close();
        }
    }

}
