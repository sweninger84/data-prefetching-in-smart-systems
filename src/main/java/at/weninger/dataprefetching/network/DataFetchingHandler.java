package at.weninger.dataprefetching.network;

import at.weninger.dataprefetching.model.NeighborData;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Handler class for DataFetchingService. Generates a new Channel object which sends the message and
 * receives the answer. Implements the Interface Callable and triggers the sending of the message in the
 * call() method, which also returns the response.
 */
public class DataFetchingHandler implements Callable{
    private final static Logger LOGGER = Logger.getLogger(DataFetchingHandler.class.getName());

    private Channel channel;
    private String message;
    private String address;

    public DataFetchingHandler(NeighborData receiver, float rate, String message) throws IOException{
        this.channel = new RateLimitedChannel(new Socket(receiver.getAddress(), receiver.getPort()), rate);
        this.message = message;
        this.address = receiver.getAddress();
    }

    @Override
    public Object call() {
        String answer = null;
        try{
            channel.send(message);
            answer = channel.receive();
            channel.close();
        }catch (IOException e){
            LOGGER.error("Error while writing or reading to/from: " + this.address);
            channel.close();
        }
        return answer;
    }
}
