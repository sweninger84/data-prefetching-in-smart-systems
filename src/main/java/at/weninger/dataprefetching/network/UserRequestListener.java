package at.weninger.dataprefetching.network;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UserRequestListener extends RequestListener {
    @Autowired
    private BeanFactory beanFactory;

    @Value("${user.port}")
    private int port;

    @Override
    protected int getPort() {
        return this.port;
    }

    @Override
    protected RequestHandler getRequestHandler() {
       return beanFactory.getBean(UserRequestHandler.class);
    }
}
