package at.weninger.dataprefetching.network;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

/**
 * Listens for requests from users or neighboring IoT devices.
 * Each new connection is passed to a RequestHandler.
 */
@Component
public abstract class RequestListener implements Runnable{
    private final static Logger LOGGER = Logger.getLogger(RequestHandler.class.getName());
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private EventExecutorGroup group;

    @Override
    public void run() {
        System.out.println("Port: " + getPort());
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();
        try {
            group = new DefaultEventExecutorGroup(10); //thread pool of 10
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new StringDecoder());
                            ch.pipeline().addLast(new StringEncoder());
                            ch.pipeline().addLast(group, getRequestHandler());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            // Bind and start to accept incoming connections.
            ChannelFuture f = b.bind(getPort()).sync();

            // Wait until the server socket is closed.
            // shut down your server.
            f.channel().closeFuture().sync();
        } catch(InterruptedException ex){
            ex.printStackTrace();
        }finally {
            close();
        }
    }

    public void close(){
        if(workerGroup != null)
            workerGroup.shutdownGracefully();
        if(bossGroup != null)
            bossGroup.shutdownGracefully();
        if(group != null)
            group.shutdownGracefully();
    }

    protected abstract int getPort();
    protected abstract RequestHandler getRequestHandler();
}
