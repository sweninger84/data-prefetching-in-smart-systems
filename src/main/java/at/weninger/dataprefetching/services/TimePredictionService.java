package at.weninger.dataprefetching.services;

import at.weninger.dataprefetching.model.ForDevice;

import java.util.List;
import java.util.Map;

/**
 * This service is aimed to providing information about what data need to be prefetched and when to start prefetching.
 */
public interface TimePredictionService {

    /**
    *    Calculates the data which need to be prefetched for a particular user,
    *    based on the data calculated by the DataPredictionService Service and considering the amount of data
    *    that can be transferred while this user is in range.
    *
    *    @param userId
    *       The userId
    *    @param dataIds
    *       The ids of the data that need to be fetched as calculated by the DataPredictionService Service
    *    @param connectionDuration
    *       How long the user will be connected to this device as calculated by the UserMobilityPredictionService Service
    *    @param transferSpeed
    *       The average data transferSpeed between the user and the device in bytes/sec
    *    @param sizeOfDataItem
    *       The average size of one data item in bytes
    *    @param arrivalTime
    *       When the user will arrive, is 0 in case user is already connected
    *    @return
    *       Map with 2 Lists:
    *       key "ForDevice.THIS": The data attributes/IDs, that can and need to be sent to the user by this device
    *       key "ForDevice.NEXT": If not all data can be sent, the data attributes/IDs that need to be sent to the user by the next device
     */
    Map<ForDevice, List<Integer>> calcFetchData(String userId, List<Integer> dataIds, long connectionDuration, int transferSpeed, int sizeOfDataItem, long arrivalTime);


    /**
     *  Calculates the time in ms until prefetching needs to start in order to get the most current data for the user.
     *
     *  @param dataIds
     *      The dataIds that need to be prefetched for this device: important for determining the size of data that need to be fetched
     *  @param arrivalTime
     *      The time the user will come into range of this device's Wi-Fi as calculated by the UserMobilityPredictionService Service
     *  @param transferSpeed
     *      The average data transferSpeed between devices in bytes/sec
     *  @param sizeOfDataItem
     *      The average size of one data item in bytes
     *  @return
     *      The time span in ms until prefetching should start, in order to get the most current data
     */
    long calcFetchTime(List<Integer> dataIds, long arrivalTime, int transferSpeed, int sizeOfDataItem);
}
