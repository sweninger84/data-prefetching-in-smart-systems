package at.weninger.dataprefetching.services.impl;

import at.weninger.dataprefetching.DataStorage;
import at.weninger.dataprefetching.model.ForDevice;
import at.weninger.dataprefetching.services.TimePredictionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TimePredictionServiceImpl implements TimePredictionService {
    private final static Logger LOGGER = Logger.getLogger(TimePredictionServiceImpl.class.getName());
    private final int firstDeviceFactor = 2;

    @Autowired
    private DataStorage dataStorage;


    @Override
    public Map<ForDevice, List<Integer>> calcFetchData(String userId, List<Integer> dataIds, long connectionDuration, int transferSpeed, int sizeOfDataItem, long arrivalTime) {
        List<Integer> dataItemsToBeSentToUser = new ArrayList<>();
        List<Integer> dataItemsForNextDevice = new ArrayList<>();
        int sendableDataItems = (int) Math.floor(((double)connectionDuration)/1000 * transferSpeed / sizeOfDataItem);
        if(sendableDataItems < 0){
            LOGGER.warn("CalcFetchData: Sendable data items < 0.");
            sendableDataItems = 0;
        }
        if(arrivalTime <= 0){
            //if user is already here, send less items because there is no time for prefetching
            sendableDataItems = (int) Math.floor((double)sendableDataItems / firstDeviceFactor);
        }
        int counter = 0;
        if(dataIds != null) {
            for (Integer id : dataIds) {
                if (counter < sendableDataItems) {
                    dataItemsToBeSentToUser.add(id);
                    counter++;
                } else {
                    dataItemsForNextDevice.add(id);
                }
            }
        }
        Map<ForDevice, List<Integer>> response = new HashMap<>();
        response.put(ForDevice.THIS, dataItemsToBeSentToUser);
        response.put(ForDevice.NEXT, dataItemsForNextDevice);
        return response;
    }

    @Override
    public long calcFetchTime(List<Integer> dataIds, long arrivalTime, int transferSpeed, int sizeOfDataItem) {
        if(arrivalTime <= 0 || transferSpeed <= 0 || sizeOfDataItem <= 0){
            LOGGER.warn("CalcFetchTime: Negative parameter given.");
            return 0;
        }
        int size = dataIds == null ? 0 : dataIds.size();
        int totalsize = sizeOfDataItem * size;
        //factor 1000 to convert to ms
        double estFetchingDuration = (double) (totalsize / (transferSpeed * 2)) * 1000;
        return arrivalTime - (long) estFetchingDuration;
    }
}
