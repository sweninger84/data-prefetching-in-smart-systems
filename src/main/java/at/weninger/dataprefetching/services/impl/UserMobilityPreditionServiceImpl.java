package at.weninger.dataprefetching.services.impl;

import at.weninger.dataprefetching.DataStorage;
import at.weninger.dataprefetching.model.NeighborData;
import at.weninger.dataprefetching.model.UserMetadata;
import at.weninger.dataprefetching.services.UserMobilityPredictionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class UserMobilityPreditionServiceImpl implements UserMobilityPredictionService {
    //in m/sec
    private double DEFAULTMOVEMENTSPEED = 0.8;

    @Value("${distance.between.devices}")
    private double distanceBetweenDevices; //in m

    @Value("${device.id}")
    int deviceId;

    @Autowired
    private DataStorage dataStorage;

    @Override
    public long getConnectionDuration(UserMetadata metadata) {
        double movementSpeed = metadata.getMovementSpeed() <= 0 ? DEFAULTMOVEMENTSPEED : metadata.getMovementSpeed();
        return (long) (distanceBetweenDevices / movementSpeed * 1000);
    }

    /*
    * Takes distance between Devices in m (half because after half the distance the user will already connect to this device),
    * divides by Movement Speed of User in m/sec then multiplies with 1000 for ms and
    * adds the Duration the User will be connected to the other device (average)
     */
    @Override
    public long getTimeUntilArrival(UserMetadata metadata) {
        return (long) ((double) getConnectionDuration(metadata) * 0.8);
    }

    @Override
    public NeighborData getNextDevice(UserMetadata metadata) {

        //The direction the user is going. Up: towards positive device Ids. Down: towards negative device ids
        boolean up = false;
        List<NeighborData> neighborsData = dataStorage.getNeighborsData();
        for(NeighborData neighborData : neighborsData){
            //a user goes up by default if possible
            if(neighborData.getId() > deviceId){
                up = true;
            }
        }
        int prevDevice = metadata.getDirectionInfo();
        if(prevDevice != -1){
            //direction Info is available
            if(prevDevice > deviceId){
                up = false;
            }else{
                up = true;
            }
        }

        int temp;
        NeighborData response = null;
        if(up){
            temp = 1000;
        }else{
            temp = -1;
        }
        for(NeighborData neighborData : neighborsData){
            if(up){
                if(neighborData.getId() > deviceId && neighborData.getId() < temp){
                    temp = neighborData.getId();
                    response = neighborData;
                }
            }else{
                if(neighborData.getId() < deviceId && neighborData.getId() > temp){
                    temp = neighborData.getId();
                    response = neighborData;
                }
            }
        }
        return response;
    }
}
