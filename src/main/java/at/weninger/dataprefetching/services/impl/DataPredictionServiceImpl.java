package at.weninger.dataprefetching.services.impl;

import at.weninger.dataprefetching.DataStorage;
import at.weninger.dataprefetching.model.*;
import at.weninger.dataprefetching.services.DataPredictionService;
import at.weninger.dataprefetching.util.ConfigReader;
import at.weninger.dataprefetching.util.JsonParser;
import com.google.gson.stream.JsonReader;
import io.netty.handler.codec.http.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


@Service
public class DataPredictionServiceImpl implements DataPredictionService {
    private final static Logger LOGGER = Logger.getLogger(DataPredictionServiceImpl.class.getName());
    private final int EARTH_RADIUS = 6371;
    private final String WLURL = "http://www.wienerlinien.at/ogd_realtime/monitor";
    private double radius = 0.5;

    @Value("${device.id}")
    private int deviceId;
    @Value("${key.development}")
    private String apiKey;
    private ArrayList<GPSbounds> gpsBounds;
    private WienerLinienStationInfo[] wlData;

    @Autowired
    private ConfigReader configReader;
    @Autowired
    private ResourceLoader resourceLoader;
    @Autowired
    private DataStorage dataStorage;
    @Autowired
    private JsonParser jsonParser;


    public void setRadius(double radius) {
        this.radius = radius;
    }

    @PostConstruct
    private void initializeData(){
        try {
            this.wlData = jsonParser.jsonToWienerLinienData(new JsonReader(new InputStreamReader(resourceLoader.getResource("classpath:wl.json").getInputStream(), "UTF-8")));;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<ResponseData> getData(UserMetadata metadata) {
        return getData(metadata, null);
    }

    @Override
    public List<ResponseData> getData(UserMetadata metadata, List<Integer> dataIds) {
        ArrayList<ResponseData> resultsList = new ArrayList<>();
        boolean idsOnly = false;
        if(dataIds == null || dataIds.size() == 0){
            idsOnly = true;
            //get needed ids from the user metadata
            dataIds = calcPlatformsInRadius(metadata.getDestinationPos(), radius);
        }
        LOGGER.info("Fetching " + dataIds.size() + " items from WL.");
        for(Integer id : dataIds){
            ResponseData dataItem;
            if(idsOnly){
                //if the data ids are missing, we only return the ids and not the data items
                dataItem = new ResponseData(id, "");
            }else{
                dataItem = new ResponseData(id, getDataFromWL(id));
            }
            resultsList.add(dataItem);
        }
        return resultsList;
    }

    @Override
    public boolean isResponsible(UserMetadata metadata) {
        if(metadata == null){
            return false;
        }
        if(gpsBounds == null){
            configReader.setReader("/gpsbounds.config");
            this.gpsBounds = configReader.getGPSbounds();
        }
        if(gpsBounds == null || deviceId > gpsBounds.size() || deviceId < 1){
            LOGGER.warn("Cannot get GPS Bounds for this device ID: " + deviceId);
            return false;
        }
        double lat = metadata.getDestinationPos().getLat();
        double lng = metadata.getDestinationPos().getLon();
        GPSbounds deviceBounds = gpsBounds.get(deviceId-1);
        return lat >= deviceBounds.getLatMin() && lat < deviceBounds.getLatMax() && lng >= deviceBounds.getLngMin() && lng < deviceBounds.getLngMax();
    }

    private String getDataFromWL(int rblNumber){
        QueryStringEncoder encoder = new QueryStringEncoder(WLURL);
        encoder.addParam("rbl",Integer.toString(rblNumber));
        encoder.addParam("sender", apiKey);
        try{
            URL url = new URL(encoder.toString());
            URLConnection uc = url.openConnection();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            uc.getInputStream(), "UTF-8"));
            StringBuilder answer = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                answer.append(inputLine);
            in.close();
            return answer.toString();
        }catch (MalformedURLException e){
            LOGGER.error("Malformed URL: Cannot connect to Wiener Linien.");
        }catch (IOException e){
            LOGGER.error("IO Exception while connecting to Wiener Linien: " + e.getMessage());
        }
        return "Data item could not be retrieved.";
    }

    /*
    * Radius in km
     */
    private List<Integer> calcPlatformsInRadius(Position location, double radius){
        List<Integer> rblNumbers = new ArrayList<>();
        if(wlData == null){
            return rblNumbers;
        }
        for(WienerLinienStationInfo stop : wlData){
            Position location2 = new Position(stop.getLatitude(), stop.getLongitude());
            if(calcDistance(location, location2) <= radius){
                Platform[] platforms = stop.getPlatforms();
                for(Platform platform : platforms){
                    rblNumbers.add(platform.getRbl());
                }
            }
        }
        return rblNumbers;
    }

    /*
    * Returns the distance in m between location1 and location2
     */
    private double calcDistance(Position location1, Position location2){
        double dLat = deg2rad(location2.getLat() - location1.getLat());
        double dLon = deg2rad( location2.getLon() - location1.getLon() );

        double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(location1.getLat())) * Math.cos(deg2rad(location2.getLat())) * Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return EARTH_RADIUS * c;
    }

    private double deg2rad(double degree){
        return Math.tan(degree * (Math.PI/180));
    }

}
