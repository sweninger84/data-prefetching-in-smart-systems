package at.weninger.dataprefetching.services;

import at.weninger.dataprefetching.model.ResponseData;
import at.weninger.dataprefetching.model.UserMetadata;

import java.util.List;

/**
 * This service provides methods to predict which data will be needed by the user.
 * As input it takes the metadata provided by the user in json format.
 */
public interface DataPredictionService {

    /**
    *   Returns the data items for the given data ids/hashes
    *
    *   @param metadata
    *      The user metadata in json format
    *   @param dataIds
    *       The Hash Values/IDs of the data to be returned
    *   @return
    *       Response Data for the given Ids/hashes
    */
    List<ResponseData> getData(UserMetadata metadata, List<Integer> dataIds);

    /**
    *   Based on the input data, a prediction algorithm calculates data required by the user.
    *   Then fetches the needed data and return them.
    *
    *   @param metadata
    *      The user metadata in json format
    *   @return
    *       Response Data required by the user
    */
    List<ResponseData> getData(UserMetadata metadata);

    /**
    * Based on the input data, this algorithm predicts, whether this device
    * is responsible for providing the data required by the user.
    * @param metadata
    *      The user metadata in json format
    *  @return
    *       True if this device is responsible for providing the data, false otherwise
     */
    boolean isResponsible(UserMetadata metadata);

}
