package at.weninger.dataprefetching.services;

import at.weninger.dataprefetching.model.NeighborData;
import at.weninger.dataprefetching.model.UserMetadata;

/**
 * This service provides methods aiming to predict where the user will go and at what speed.
 * It needs the user metadata, especially the GPS coordinates of the user.
 * It provides methods to calculate when the user will come into range of a device’s Wi-Fi signal and how long the user will stay in range.
 */
public interface UserMobilityPredictionService {

    /**
    *   Calculates how long a user will be range of the device's Wi-Fi signal
    *
    *   @param metadata
    *       The user metadata
    *   @return
    *       Time in ms
     */
    long getConnectionDuration(UserMetadata metadata);

    /**
    *   Calculates when the user will enter this device's Wi-Fi signal
    *
    *   @param metadata
    *       The user metadata
    *   @return
    *       Time in ms until the user comes into range
     */
    long getTimeUntilArrival(UserMetadata metadata);

    /**
    *   Calculates the next device the user will come into range
    *
    *   @param metadata
    *       The user metadata
    *   @return
    *       The IP address of the next device
     */
    NeighborData getNextDevice(UserMetadata metadata);


}
