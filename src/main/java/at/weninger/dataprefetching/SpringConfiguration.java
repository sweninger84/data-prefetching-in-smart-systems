package at.weninger.dataprefetching;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.util.Properties;


@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = {"at.weninger.dataprefetching"})
public class SpringConfiguration {
    private static String deviceId = "1";

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        Properties properties = new Properties();
        properties.setProperty("device.id", deviceId);
        configurer.setProperties(properties);
        return configurer;
    }

    public static void setDeviceId(String newDeviceId){
        deviceId = newDeviceId;
    }
}

