# Device Application for my Bachelor Thesis: "Data Prefetching in Smart Systems" #

Internet of Things (IoT) devices, which become increasingly popular, generate an enormous amount of data,
which needs to be processed and stored. An alternative to storing the data in the cloud is to keep them locally on
the device. This might be useful if the data contain sensitive information or the devices
do not have access to the Internet. However, a big drawback of this solution is reduced
accessibility of data, because the data might need to be fetched from multiple locations
over a mobile connection with fluctuating quality, leading to delays. Prefetching of data,
that is fetching data before they are requested, is a way to overcome this problem.

This application provides a prefetching solution for IoT devices in a
fully distributed environment, meaning that each device is connected only to its direct
neighbours. This implementation focuses on providing a template for prefetching which
is meant to be extended using specific prediction algorithms.

In order to achieve prefetching of data, the IoT devices in the network communicate
with each other via two different message types. One message type is for requesting data
items, called GET message. The other message type, called FETCH message, is used to
let the receiving device know that a user will arrive shortly and that it should start to
prefetch certain data items.

For this setup to work, each IoT device needs to set up a WI-FI network with the static IP address 192.168.1.1 assigned to the wireless port. The user will connect to the device via this IP address and request data. We also developed a simple mobile app for an Android mobile phone (https://bitbucket.org/sweninger84/data-prefetching-in-smart-systems-client-app). This mobile app sends user metadata to the connected network and displays the response.

In this implementation the requested data are live public transport timetable information from the Wiener Linien API.

### Deployment ###

- mvn clean install  -> build project inclusive dependencies
- cd to target folder 
- java -cp data-prefetching-in-smart-systems-1.0-SNAPSHOT-jar-with-dependencies.jar at.weninger.dataprefetching/Server "x"  -> x: the server id (for this setup values from 1-6 are allowed)